package com.alexandergolovinov.learnandroidmemory.sharedPreferences;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.alexandergolovinov.learnandroidmemory.R;

public class SharedPrefActivity extends Activity {

    private TextView mTextWelcome;
    private EditText mEditName;
    private Button mButtonSubmit;

    /*Unique identifier for the SharedPreferences object
    Best practice is to use package, because it is unique across all apps in the phone*/
    public static final String USER_DATA = "com.alexandergolovinov.learnandroidmemory.sharedPreferences.USER_DATA";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sharedpreferences_activity);

        mTextWelcome = (TextView) findViewById(R.id.text_welcome);
        mEditName = (EditText) findViewById(R.id.edit_name);
        mButtonSubmit = (Button) findViewById(R.id.btn_submit);

        String inputNameStored = getSharedPreferences(USER_DATA, MODE_PRIVATE).getString("inputText", null);
        if (inputNameStored != null) {
            mTextWelcome.setText("Welcome, " + inputNameStored);
        } else {
            mTextWelcome.setText(null);
        }

        mButtonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputText = mEditName.getText().toString().trim();

                mTextWelcome.setText("Welcome, " + inputText);

                SharedPreferences.Editor editor = getSharedPreferences(USER_DATA, MODE_PRIVATE).edit();
                editor.putString("inputText", inputText).apply();

            }
        });

    }
}
