package com.alexandergolovinov.learnandroidmemory.SQLite;

import android.database.SQLException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.alexandergolovinov.learnandroidmemory.R;

public class DataActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);

        TextView mTextDisplayData = (TextView) findViewById(R.id.text_display_sqlite_data);

        try {
            ContactsDB db = new ContactsDB(this);
            db.open();
            mTextDisplayData.setText(db.getData());
            db.close();
        } catch (SQLException e) {
            Log.d("SQLite", e.toString());
        }


    }
}
