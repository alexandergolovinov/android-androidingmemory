package com.alexandergolovinov.learnandroidmemory.SQLite;

import android.app.Activity;
import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.alexandergolovinov.learnandroidmemory.R;

public class SQLiteActivity extends Activity {

    private static final String TAG = "SQLite";

    private EditText mEditTextName;
    private EditText mEditTextPhone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sqlite_activity);

        mEditTextName = (EditText) findViewById(R.id.edit_sqlite_name);
        mEditTextPhone = (EditText) findViewById(R.id.edit_sqlite_phone);

    }

    public void btnSubmit(View v) {
        final String name = mEditTextName.getText().toString().trim();
        final String cell = mEditTextPhone.getText().toString().trim();

        try {
            ContactsDB db = new ContactsDB(this);
            db.open();
            db.createEntry(name, cell);
            db.close();
            Toast.makeText(this, "Succesfully saved!", Toast.LENGTH_SHORT).show();
            mEditTextName.setText("");
            mEditTextPhone.setText("");
        } catch (SQLException e) {
            Log.d(TAG, e.toString());
        }

    }

    public void btnEditData(View v) {
        try {
            ContactsDB db = new ContactsDB(this);
            db.open();
            db.updateEntry("1", "Alexander Golovinov", "15-44-33-22");
            db.close();
        } catch (SQLException e) {
            Log.d(TAG, e.toString());
        }
    }

    public void btnShowData(View v) {
        startActivity(new Intent(this, DataActivity.class));
    }


    public void btnDeleteData(View v) {
        try {
            ContactsDB db = new ContactsDB(this);
            db.open();
            db.deleteEntry("1");
            db.close();
        } catch (SQLException e) {
            Log.d(TAG, e.toString());
        }
    }


}
