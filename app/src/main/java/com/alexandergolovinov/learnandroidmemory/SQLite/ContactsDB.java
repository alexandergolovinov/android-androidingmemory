package com.alexandergolovinov.learnandroidmemory.SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

public class ContactsDB {

    /**
     * Columns for the DB Table.
     * (NB! Underscore should be present in every name)
     */
    private static final String KEY_ROW_ID = "_ID";
    private static final String KEY_NAME = "person_name";
    private static final String KEY_CELL = "_cell";

    private static final String DATABASE_NAME = "ContactsDB";
    private static final String DATABASE_TABLE = "ContactsTable";
    private static final int DATABASE_VERSION = 1;

    private DBHelpter ourHelpter;
    private final Context outContext;
    private SQLiteDatabase ourDatabase;

    public ContactsDB(Context outContext) {
        this.outContext = outContext;
    }


    protected ContactsDB open() throws SQLException {
        ourHelpter = new DBHelpter(outContext);
        ourDatabase = ourHelpter.getWritableDatabase();
        return this;
    }

    protected void close() {
        ourHelpter.close();
    }

    /**
     * Creating new entry in DB
     * @param name for the entity
     * @param cell for the entity
     * @return
     */

    protected long createEntry(final String name, final String cell) {
        ContentValues cv = new ContentValues();
        cv.put(KEY_NAME, name);
        cv.put(KEY_CELL, cell);
        //It returns the number of items inserted
        return ourDatabase.insert(DATABASE_TABLE, null, cv);
    }

    /**
     * Here you can return instance of the class in a collection.
     * Example is simplified to String
     *
     * @return text information from DB
     */
    protected String getData() {
        String[] columns = new String[]{KEY_ROW_ID, KEY_NAME, KEY_CELL};
        Cursor c = ourDatabase.query(DATABASE_TABLE, columns, null, null, null, null, null);

        StringBuilder result = new StringBuilder();

        int iRowID = c.getColumnIndex(KEY_ROW_ID);
        int iName = c.getColumnIndex(KEY_NAME);
        int iCell = c.getColumnIndex(KEY_CELL);

        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
            result.append(c.getString(iRowID) + ": ")
                    .append(c.getString(iName) + " ")
                    .append(c.getString(iCell) + "\n");
        }

        c.close();
        return result.toString();
    }

    /**
     * Delete entry in DB where id equals inserted id
     * @param rowId is the parameter for the row
     * @return number of elements removed
     */

    protected long deleteEntry(final String rowId) {
        return ourDatabase.delete(DATABASE_TABLE, KEY_ROW_ID + "=?", new String[]{rowId});
    }

    /**
     * Update existing entry in the DB
     * @param rowID is the parameter for the row to update
     * @param name new name for entity
     * @param cell new cell for entity
     * @return number of elements removed
     */
    protected long updateEntry(final String rowID, final String name, final String cell) {
        ContentValues cv = new ContentValues();
        cv.put(KEY_ROW_ID, rowID);
        cv.put(KEY_NAME, name);
        cv.put(KEY_CELL, cell);

        return ourDatabase.update(DATABASE_TABLE, cv, KEY_ROW_ID + "=?", new String[]{rowID});
    }


    /**
     * Helper class to initialize DB
     * onCreate method is called if DB is empty
     * onUpgrade method is called if DB_VERSION changes
     */
    private class DBHelpter extends SQLiteOpenHelper {

        public DBHelpter(@Nullable Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            /*
                CREATE TABLE ContactsTable (_id INTEGER PRIMARY KEY AUTOINCREMENT,
                                    person_name TEXT NOT NULL,
                                    _cell TEXT NOT NULL);
             */

            String sqlCode = "CREATE TABLE " + DATABASE_TABLE + "(" + KEY_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                                             + KEY_NAME + " TEXT NOT NULL, "
                                             + KEY_CELL + " NOT NULL);";

            db.execSQL(sqlCode);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            //When we change DB_VERSION, then drop table
            db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
            onCreate(db);
        }
    }
}
