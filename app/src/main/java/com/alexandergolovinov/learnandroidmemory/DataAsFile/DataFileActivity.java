package com.alexandergolovinov.learnandroidmemory.DataAsFile;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alexandergolovinov.learnandroidmemory.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.StringTokenizer;

public class DataFileActivity extends Activity {

    private static final String FILE_NAME = "data.txt";
    private EditText mTextUserName;
    private EditText mTextUserSurname;
    private TextView mTextDisplayData;
    private List<Person> peopleData;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.data_store_activity);

        mTextUserName = (EditText) findViewById(R.id.edit_username);
        mTextUserSurname = (EditText) findViewById(R.id.edit_usersurname);
        mTextDisplayData = (TextView) findViewById(R.id.text_display_file_data);

        peopleData = new ArrayList<>();

        loadData();

    }

    public void addData(View view) {
        String name = mTextUserName.getText().toString().trim();
        String surname = mTextUserSurname.getText().toString().trim();
        if (TextUtils.isEmpty(name) || TextUtils.isEmpty(surname)) {
            Toast.makeText(this, "Fill all fields", Toast.LENGTH_SHORT).show();
        }
        peopleData.add(new Person(name, surname));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void setTextToTextView() {
        Optional<String> userDataText = peopleData.stream().map(p -> p.getName() + " " + p.getLastname())
                .reduce((p1, p2) -> p1 + " " + p2 + "\n");
        if (userDataText.isPresent()) {
            mTextDisplayData.setText(userDataText.get());
        } else {
            mTextDisplayData.setText("DataActivity is not present");
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void loadData() {
        peopleData.clear();

        File file = getApplicationContext().getFileStreamPath(FILE_NAME);
        String lineFromLine;

        if (file.exists()) {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(openFileInput(FILE_NAME)))) {
                while ((lineFromLine = reader.readLine()) != null) {
                    StringTokenizer tokens = new StringTokenizer(lineFromLine, ",");
                    Person person = new Person(tokens.nextToken(), tokens.nextToken());
                    peopleData.add(person);
                }
                setTextToTextView();
            } catch (IOException e) {
                Log.d("DATA", "Error during the data load!");
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void saveDataToFile(View view) {
        try {
            FileOutputStream file = openFileOutput(FILE_NAME, MODE_PRIVATE);
            OutputStreamWriter outputFile = new OutputStreamWriter(file);

            peopleData.stream().forEach(p -> {
                try {
                    outputFile.write(p.getName() + "," + p.getLastname() + "\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            outputFile.flush();
            outputFile.close();
            Toast.makeText(this, "Succesfully saved data!", Toast.LENGTH_SHORT).show();

        } catch (IOException e) {
            Log.d("DATA", "Error during the save error + " + e.toString());
        }
    }

    public void removeData(View view) {
        File file = getApplicationContext().getFileStreamPath(FILE_NAME);
        if (file.exists()) {
            final boolean removedSuccessfully = file.delete();
            if (removedSuccessfully) {
                Toast.makeText(this, "File removed succesfully", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Error during file remove", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
