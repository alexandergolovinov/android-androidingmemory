package com.alexandergolovinov.learnandroidmemory.AsyncTask;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alexandergolovinov.learnandroidmemory.MainActivity;
import com.alexandergolovinov.learnandroidmemory.R;

import java.util.Random;

public class AsyncActivity extends Activity {

    private EditText mEditTextEnterNumber;
    private Button mBtnRollDice;
    private TextView mTextDisplayNums;
    private int numEntered;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.async_activity);

        mEditTextEnterNumber = (EditText) findViewById(R.id.edit_enter_num);
        mBtnRollDice = (Button) findViewById(R.id.btn_roll_dice);
        mTextDisplayNums = (TextView) findViewById(R.id.text_display_numbers);

        mTextDisplayNums.setVisibility(View.GONE);
        mBtnRollDice.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                numEntered = Integer.valueOf(mEditTextEnterNumber.getText().toString().trim());
                new ProcessDiceInBackground().execute(numEntered);
            }

        });
    }

    class ProcessDiceInBackground extends AsyncTask<Integer, Integer, String> {

        ProgressDialog dialog;

        //BEFORE Execution
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(AsyncActivity.this);
            dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            dialog.setMax(numEntered);
            dialog.show();
        }

        //RUN the process
        @Override
        protected String doInBackground(Integer... params) {
            int one = 0;
            int twos = 0;
            int threes = 0;
            int fours = 0;
            int fives = 0;
            int sixes = 0;
            int randomNum;


            Random random = new Random();

            for (int i = 0; i < params[0]; i++) {
                randomNum = random.nextInt(6) + 1;
                publishProgress(i);
                switch (randomNum) {
                    case 1:
                        one++;
                        break;
                    case 2:
                        twos++;
                        break;
                    case 3:
                        threes++;
                        break;
                    case 4:
                        fours++;
                        break;
                    case 5:
                        fives++;
                        break;
                    case 6:
                        sixes++;
                        break;
                    default:
                        break;
                }

            }
            return "Results: " + one + ", " + twos + ", " + threes + ", " + fours + ", " + fives + ", " + sixes;
        }

        //DURING the process
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            dialog.setProgress(values[0]);
        }

        //AFTER executing
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();

            mTextDisplayNums.setVisibility(View.VISIBLE);
            mTextDisplayNums.setText(s);
            Toast.makeText(AsyncActivity.this, "Process done!", Toast.LENGTH_LONG).show();
        }
    }

}
